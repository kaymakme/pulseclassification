/*
    This software is Copyright by the Board of Trustees of Michigan
    State University (c) Copyright 2017.

    You may use this software under the terms of the GNU public license
    (GPL).  The terms of this license are described at:

     http://www.gnu.org/licenses/gpl.txt

     Authors:
             Ron Fox
             Jeromy Tompkins 
	     NSCL
	     Michigan State University
	     East Lansing, MI 48824-1321
*/

/** @file:  DtRange.h 
 *  @brief: Header with DTRange data types.
 */
#ifndef DTRANGE_MODIFIED_H
#define DTRANGE_MODIFIED_H

#include "functions.h"
#include <stdlib.h>
#include <iostream>
#include <vector>

/**
 *  This is the format of an event.  Note that the arrays shown have the
 *  number of elements that there are pulses in the generated event:
 *
 */



struct Event {
    uint32_t               s_isDouble;           // True if event is double.
    DDAS::HitExtension     s_fitinfo;            // Fit results.
    
    double                 s_actualOffset;       // Actual offset.
    DDAS::PulseDescription s_pulses[];           // One or two elements.
};



/*!
 *  This is the format of an event.  Note that the arrays shown have the
 *  number of elements that there are pulses in the generated event:
 *
 */

template<typename c, typename i1, typename i2>
c
add(i1 from, i1 to, i2 addend);



double randomize(double low, double hi);

///////cagri extra
std::vector<uint16_t> generateDoublePulseNoRandom(double o, double a1, double k1, double k2, double a2, double k3, double k4, double pos1, double pos2, bool noise, bool cut, int sampleSize);
std::vector<uint16_t> generateSinglePulseNoRandom(double a, double k1, double k2, double o, double pos1, bool noise, bool cut, int sampleSize);


std::vector<uint16_t> generateSinglePulseRandom(double a, double k1, double k2, double o, bool noise, int sampleSize);
std::vector<uint16_t> generateSinglePulseRandomCutOff(double a, double k1, double k2, double o, bool noise, int sampleSize);
std::vector<uint16_t> generateDoublePulseRandomCutOff(double o, double a1, double k1, double k2, double a2, double k3, double k4, double dtmin, double dtmax, bool noise, int sampleSize);
std::vector<uint16_t> generateDoublePulseRandom(double o, double a1, double k1, double k2, double a2, double k3, double k4, double dtmax, bool noise, int sampleSize);
////////////////////////////////////////////////////////////////////////////////////////////////////
std::vector<uint16_t> generatePulse(double a, double k1, double k2, double o, double t0, size_t n);


Event * doublePulseEvent(std::vector<uint16_t>& trace, double o, double a1, double k1, double k2, double a2, double k3, double k4, double dtmax);


Event * singlePulseEvent(std::vector<uint16_t> & trace, double o, double a, double k1, double k2);

int convertInt(const char* src, const char* msg);

double convertDouble(const char* src, const char* msg);

void usage(std::ostream& str, const char* msg);

#endif