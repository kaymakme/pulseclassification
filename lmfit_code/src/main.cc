#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <string>     // std::string, std::to_string
#include <cstring>  
#include <fstream>
#include <sstream>
#include "lmfit.h"
#include "functions.h"
#include "dtrange_modified.h"
#include <sys/stat.h>
#include <utility>
#include <chrono>
#include <ctime>
//#include "dtw.h"
//#include "haar.cpp"
# include <cmath>
#include <cstdlib>
#include <math.h>


using namespace std;
using namespace DDAS;  
 
  

std::vector<std::vector<uint16_t> > readBinary(ifstream& file, int count) {
  
  std::vector<std::vector<uint16_t> > vecs;
  if (file.is_open())
  {
     while (file.eof() == false && (count > 0 || count < 0)) {
      uint32_t size;
      file.read ((char*)&size, sizeof(size));
      std::vector<uint16_t> vec(size);
      file.read((char *)vec.data(), sizeof(uint16_t) * size);
      vecs.push_back(vec);
      count = count - 1;
     }   
  }

  return vecs;
}


template <class myType>
void writeSignal(std::vector<myType> v, string filename) {
  ofstream file1(filename);
  //print the values into files in order to view the plotted synthetic curve
  for(int count=0;count<v.size();count++)
  {
    file1 << v[count] << " ";
      //fwrite(&(i1), sizeof(int),1,(ptr_file_1));
      //fwrite(&(i2), sizeof(int),1,(ptr_file_2));

  }  

  file1.close();
}



template <class myType>
std::pair<double, double> findMaxMin(std::vector<myType> v) {
 double max = -99999;
 double min = 99999;

 for (int i = 0; i < v.size(); i++) {
  if (max < v[i]) {
    max = v[i];
  }
  if(min > v[i]) {
    min = v[i];
  }
}

return std::make_pair (min,max);
}

template <class myType>
void normalizeSignals(std::vector<myType> *vecs, int size) {


 for (int i = 0; i < size; i++) {
  std::pair<double, double> maxmin = findMaxMin(vecs[i]);
  for (int j = 0; j < vecs[i].size(); j++) {
    vecs[i][j] = (vecs[i][j] - maxmin.first)/(maxmin.second - maxmin.first);
  }
}
}

template <class myType>
void ZnormalizeSignal(std::vector<myType>& vec) {
  double mean = 0;
  double std = 0;
  for (int j = 0; j < vec.size(); j++) {
    mean = mean + vec[j];
  } 
  mean = mean / vec.size();
  for (int j = 0; j < vec.size(); j++) {
    std = std + (vec[j] - mean) * (vec[j] - mean);
  } 
  std = sqrt(std/vec.size());

  for (int j = 0; j < vec.size(); j++) {
    vec[j] = (vec[j] - mean)/std;
  }

}

template <class myType>
void ZnormalizeSignals(std::vector<myType> *vecs, int size) {
 for (int i = 0; i < size; i++) {
   ZnormalizeSignal(vecs[i]);
 }
}

template <typename T>
std::vector<T> readFile(string filename) {
  // open file    
  ifstream inputFile(filename);
  std::vector<T> v;
// test file open   
  if (inputFile) {        
    T value;

    // read the elements in the file into a vector  
    while ( inputFile >> value ) {
      v.push_back(value);
    }

  }
  return v;
}

pair<fit1Info,fit2Info> fitTrace(std::vector<uint16_t> &vec, uint16_t saturation) {
    //DDAS::lmfit2(info2+i, traces[i], std::make_pair (0,sampleSize-1), info1+i);
  fit2Info info2;
  fit1Info info1;   
  DDAS::lmfit1(&info1, vec, std::make_pair (0,vec.size()-1),saturation);    
  DDAS::lmfit2(&info2, vec, std::make_pair (0,vec.size()-1), &info1,saturation);

  return std::make_pair(info1, info2);
}

std::vector<pair<fit1Info,fit2Info> > fitData(std::vector<std::vector<uint16_t> >&allData, uint16_t saturation) {
  std::vector<pair<fit1Info,fit2Info> > results(allData.size());
    for(int i = 0; i < allData.size(); i++) { //allData->size()
      /*
      if (i % 100000 == 0) {
        cout << i << endl;
      }
      */
      pair<fit1Info,fit2Info> myPair = fitTrace(allData.at(i),saturation);
      results[i] = myPair;
    }
    return results;

  }

  void printFittingResults(std::vector<pair<fit1Info,fit2Info> > results, string outFilename) {
    ofstream file1(outFilename);
    for(int i = 0; i < results.size(); i++) { 
      double arr[20];
      arr[0] = results[i].first.iterations;
      arr[1] = results[i].first.fitStatus;
      arr[2] = results[i].first.chiSquare;
      arr[3] = results[i].first.pulse.position;
      arr[4] = results[i].first.pulse.amplitude;
      arr[5] = results[i].first.pulse.steepness;
      arr[6] = results[i].first.pulse.decayTime;
      arr[7] = results[i].first.offset;
      arr[8] = results[i].second.iterations;
      arr[9] = results[i].second.fitStatus;
      arr[10] = results[i].second.chiSquare;
      arr[11] = results[i].second.pulses[0].position;
      arr[12] = results[i].second.pulses[0].amplitude;
      arr[13] = results[i].second.pulses[0].steepness;
      arr[14] = results[i].second.pulses[0].decayTime;
      arr[15] = results[i].second.pulses[1].position;
      arr[16] = results[i].second.pulses[1].amplitude;
      arr[17] = results[i].second.pulses[1].steepness;
      arr[18] = results[i].second.pulses[1].decayTime;
      arr[19] = results[i].second.offset;  

      for (int j = 0; j < 20; j++) {
        /*
        if (isnan(arr[j])) {
          arr[j] = -999999;
        }
        */
        if (j < 19) {
          file1 << arr[j] << " ";
        }
        else {
          if (i != results.size() - 1 ) {
            file1 << arr[j] << endl;
          }
          else {
            file1 << arr[j];
          }
        }
      }  

    }
    file1.close();
    
  }
  std::vector<std::vector<uint16_t> > readRealData(string filename) {
    std::vector<uint16_t> fullVec = readFile<uint16_t>(filename);

    uint16_t size = fullVec[0];
    int ctr = 0;
    std::vector<std::vector<uint16_t> > res;
    std::vector<uint16_t> temp_vec;
    for (int i = 1; i < fullVec.size(); i++) {

      if(ctr == size) {
        res.push_back(temp_vec);  
        size =  fullVec[i];  
        temp_vec.clear(); 
        ctr = -1;
      }
      else {
        temp_vec.push_back(fullVec[i]);
      }
      
      ctr++;  
    }
    return res;
  }


  void binaryPrint(uint16_t num) {
    for (int i = 0; i < 16; i++) {
      printf("%d", (num & 0x8000) >> 15);
      num <<= 1;
    }
  }

template <class myType>
  vector<double> convertDouble(vector<myType> v) {
    std::vector<double> v_d(v.size());
    for (int i = 0; i < v.size(); i++) {
      v_d[i] = double(v[i]);
    }
    return v_d;
  }

  std::vector<double>* createSignals(int count, int sampleSize, bool doublePulse, bool mergedDouble, bool noise) {
 // generate random
  //declaring the parameters that will be used to generate synthetic pulses for fitting     
    double o;
    double a1, a2;
    double k1, k2, k3, k4;
    double dthigh;

    dthigh = 100;
    int dtmin = 15;
    int size = sampleSize;

    vector<double>* traces;
    if (doublePulse && mergedDouble) {
      traces = new vector<double>[count * 3] ;
    }
    else if (mergedDouble || doublePulse) {
      traces = new vector<double>[count * 2] ;
    }
    else {
      traces = new vector<double>[count] ;
   // generate signals
    }
    for(int i = 0; i < count; i++) {
      vector<uint16_t> v = generateSinglePulseRandomCutOff(o, a1, k1, k2, noise, size);
      if (doublePulse && mergedDouble) {
       vector<uint16_t> t2 = generateDoublePulseRandomCutOff(o, a1, k1, k2, a2, k3, k4, dthigh, dtmin, noise, size);
       vector<double> t2_d = convertDouble(t2);
       traces[i + count] = t2_d;
       vector<uint16_t> t3 = generateDoublePulseRandomCutOff(o, a1, k1, k2, a2, k3, k4, dtmin,0, noise, size);
       vector<double> t3_d = convertDouble(t3);
       traces[i + 2*count] = t3_d;
     }
     else if(mergedDouble || doublePulse) {
      vector<uint16_t> vec;
      if (mergedDouble) {
        vec = generateDoublePulseRandomCutOff(o, a1, k1, k2, a2, k3, k4, dtmin,10, noise, size);
      }else {
        vec = generateDoublePulseRandomCutOff(o, a1, k1, k2, a2, k3, k4, dthigh, dtmin, noise, size);
      }

      vector<double> vec_d = convertDouble(vec);
      traces[i + count] = vec_d;
    }

    traces[i] = convertDouble(v);
    
  }  
  return traces;
}

void createSignalsandWrite(int count, int sampleSize, string folder) {
  vector<double>* traces = createSignals(count, sampleSize, true,true, true);
      //normalizeSignals(traces, count);
  for(int i = 0; i < 3*count; i++) {
    string filename = folder + "/trace_" + to_string(i) + ".txt"; 
    writeSignal(traces[i], filename);
  }
}



void fit1Test(int count, int sampleSize) {
 //declaring the parameters that will be used to generate synthetic pulses for fitting     
  double o;
  double a1;
  double k1, k2;
  o = 100;
  a1 = 200;
  k1 = 0.5;
  k2 = 0.05;

  vector<uint16_t> traces[count];

  for(int i = 0; i < count; i++) {
    traces[i] = generateSinglePulseRandom(o, a1, k1, k2, false, sampleSize);
  }
  fit1Info info[count];

  //auto t1 = chrono::high_resolution_clock::now();
  for(int i = 0; i < count; i++) {
    DDAS::lmfit1(info+i, traces[i], std::make_pair (0,sampleSize-1));
  }
  //auto t2 = chrono::high_resolution_clock::now();

  //std::cout << "fit1Test took "
  //<< chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count()
  //<< " milliseconds for count = " << count << endl;
  double avgChiSq = 0;
  for(int i = 0; i < count; i++) {
    avgChiSq = avgChiSq + info[i].chiSquare;
  }
  avgChiSq = avgChiSq/count;
  cout << "average chi square: " << avgChiSq << endl;

}  

void fit2Test(string infile, int count) {
  int sampleSize = 250;
  ifstream file (infile, ios::in | ios::binary);
  std::vector<std::vector<uint16_t>> fullVec = readBinary(file, count);
  cout << "number of traces to fit: " << fullVec.size() << endl;
  int saturation = 16383;

  fit2Info info2[count];
  fit1Info info1[count];
  auto t1 = chrono::high_resolution_clock::now();
  for(int i = 0; i < count; i++) {
    DDAS::lmfit2(info2+i, fullVec[i], std::make_pair (0,sampleSize-1), info1+i,saturation);
  }
  auto t2 = chrono::high_resolution_clock::now();

  std::cout << "fit2Test took "
  << chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count()
  << " milliseconds for count = " << count << endl;

} 


std::vector<std::vector<double> > read_random_params_double(string filename) {
  std::ifstream inFile(filename);

  std::vector<std::vector<double> > fullVec;

  while (inFile.eof() == false) {
    std::vector<double> vec;
    double o;
      double a1, a2;
    double k1, k2, k3, k4;
    double pos1, pos2;
    double chi;

    inFile >> o >> chi >> pos1 >> a1 >> k1 >> k2 >> pos2 >> a2 >> k3 >> k4;
    vec.push_back(o); vec.push_back(pos1);
    vec.push_back(a1); vec.push_back(abs(k1));
    vec.push_back(abs(k2)); vec.push_back(pos2);
    vec.push_back(a2); vec.push_back(abs(k3));
    vec.push_back(abs(k4));
    if (vec[3] < 0.5) {
      vec[3] = vec[3] * 3;
    }
    fullVec.push_back(vec);

  }
  return fullVec;

}

std::vector<std::vector<double> > read_random_params_single(string filename) {
  std::ifstream inFile(filename);

  std::vector<std::vector<double> > fullVec;

  while (inFile.eof() == false) {
    std::vector<double> vec;
    double o;
      double a1;
    double k1, k2;
    double pos1;
    double chi;

    inFile >> o >> chi >> pos1 >> a1 >> k1 >> k2;
    vec.push_back(o); vec.push_back(pos1);
    vec.push_back(a1); vec.push_back(abs(k1));
    vec.push_back(abs(k2));
    if (vec[3] < 0.5) {
      vec[3] = vec[3] * 3;
    }
    fullVec.push_back(vec);

  }
  return fullVec;

}

std::vector<std::vector<double> > read_random_params_double2(string filename) {
  std::ifstream inFile(filename);

  std::vector<std::vector<double> > fullVec;

while (!inFile.eof()) { 
    std::vector<double> vec;
    string line;
    getline(inFile,line);
    istringstream sin(line);
    double inArr[20];
    for (int i = 0; i < 20; i++) {
      sin >> inArr[i];
    } 

    vec.push_back(inArr[19]); vec.push_back(inArr[11]);
    vec.push_back(inArr[12]); vec.push_back(inArr[13]);
    vec.push_back(inArr[14]); vec.push_back(inArr[15]);
    vec.push_back(inArr[16]); vec.push_back(inArr[17]);
    vec.push_back(inArr[18]); 
    fullVec.push_back(vec);


  }
  return fullVec;

}

std::vector<std::vector<double> > read_random_params_single2(string filename) {
  std::ifstream inFile(filename);

  std::vector<std::vector<double> > fullVec;
  while (!inFile.eof()) { 
    std::vector<double> vec;
    string line;
    getline(inFile,line);
    istringstream sin(line);
    double inArr[20];
    for (int i = 0; i < 20; i++) {
      sin >> inArr[i];
    } 
    vec.push_back(inArr[7]); vec.push_back(inArr[3]);
    vec.push_back(inArr[4]); vec.push_back(inArr[5]);
    vec.push_back(inArr[6]); 

    fullVec.push_back(vec);
  }
  return fullVec;

}


void writeBinary(std::vector<std::vector<uint16_t> > samples, string filename) {
  std::ofstream writeFile;
  writeFile.open(filename, std::ios::out | std::ios::binary);

  for (int i = 0; i < samples.size(); i++) {
    uint32_t size = samples[i].size();
    writeFile.write((char*)&size, sizeof(size));
    writeFile.write((char *)samples[i].data(), sizeof(uint16_t) * size);
  }
}
int main(int argc, char** argv)
{


    string in = argv[1]; //file
    string out = argv[2]; //folder
    cout << "in: " << in << endl;
    cout << "out: " << out << endl;
    long long start;
    long long end;
    start = stoll(argv[3]); // inclusive
    end = stoll(argv[4]); // exclusive
    int read_count = 50000;
    read_count = stoi(argv[5]); // exclusive

    long long count = (end - start);
    std::vector<pair<fit1Info,fit2Info> > fittingResultsFull;
    cout << std::to_string(start) << "-" << std::to_string(end) << endl; 
    cout << "Starting... count:" << std::to_string(count) << endl;
    ifstream file (in, ios::in | ios::binary);
    int ctr = 0;
    long long cursor = (1500 * 2 + 4) * start;
    file.seekg(cursor, ios::beg); // move to the start
    if (count < read_count || read_count < 1) {
         read_count = count;
    }
    for (int j = 0; j < count; j = j + read_count) { // do the operation per 50k signals
      if ((j + read_count) > count) {
         read_count = count - j;
      }
      // continue reading
      std::vector<std::vector<uint16_t>> traces = readBinary(file, read_count);
      // weirdly the last one is all zero. Messy fix.
      if (file.eof() == true && traces[traces.size()-1][2] == 0) {
        traces.erase (traces.begin()+traces.size()-1);
      }
      // get the fitting results for the current batch of traces
      std::vector<pair<fit1Info,fit2Info> > fittingResults = fitData(traces,16383);
      // merge the results
      fittingResultsFull.insert(fittingResultsFull.end(),fittingResults.begin(),fittingResults.end());

    }
    string fileOut = "";
    fileOut = fileOut + out + "/results_" + std::to_string(start) + "-" + std::to_string(end) + ".txt";
    printFittingResults(fittingResultsFull, fileOut);

    file.close();
    
    
    exit(EXIT_SUCCESS);

  }
