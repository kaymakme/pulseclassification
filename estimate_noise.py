#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Aug 18 14:44:41 2019

@author: cagri
"""

from helper import read_binary, detect_noise,plot_info_for_noise
 
import numpy as np


def main():
    import argparse
    # create parser for command-line arguments
    parser = argparse.ArgumentParser(description='Estimate the std and mean values from the given data')
    parser.add_argument('in_bin_file', metavar='binary_file',
        type=str,
        help='Input file name which contains trace data (binary file)')
    parser.add_argument('out_file', metavar='out_file',
        type=str,
        help='Output file for the noise plot') 
    parser.add_argument('-c', '--count', metavar='count', default=[50000], nargs=1,
        type=int,
        help='Number of traces to use to estimate the noise \n  \
        (using a lot of data may increase the run time) ')
    
    # parse command-line arguments
    args = parser.parse_args() 
    
    # read the traces from the binary file
    traces = read_binary(args.in_bin_file, args.count[0], False)
    # calculate the noise parameters from the data
    mean, std,diff_arr = detect_noise(traces, return_dif_array=True)
    print('mean: {}, std: {}'.format(mean,std))
    # plot the histogram and params to a file
    plot_info_for_noise(mean, std, diff_arr, args.out_file)

if __name__ == "__main__":
    main()