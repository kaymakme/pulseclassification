#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 21 02:26:31 2019

@author: cagri
"""
import numpy as np

def logistic(A, k, x1, x):
    return A/(1+np.exp(-k*(x-x1)))

def decay(A, k, x1, x):
    return A*(np.exp(-k*(x-x1)))

def single_pulse(x,A1, k1, k2, x1, C):
    return (logistic(A1, k1, x1, x)  * decay(1.0, k2, x1, x)) + C    

def double_pulse(x,A1,  k1,  k2,  x1, A2,  k3,  k4,  x2,C):
    
    p1 = single_pulse(x,A1, k1, k2, x1, C)
    p2 = single_pulse(x,A2, k3, k4, x2, 0.0)
    return p1 + p2


def generate_single_pulse(pos,a, k1, k2, o, n, noise=False,mean=0, std=3.38):
    trace = np.ndarray((n))
    for i in range(n):
        t = i
        trace[i] = single_pulse(t,a, k1, k2, pos, o)
    if noise:
        trace = add_noise(trace, mean=mean, std=std)
    return trace.astype(int)

def generate_double_pulse(pos1,a1, k1, k2,pos2, a2,k3,k4, o, n, noise=False,mean=0, std=3.38):
    trace = np.ndarray((n))
    for i in range(n):
        t = i
        trace[i] = double_pulse(t,a1, k1, k2, pos1,a2,k3,k4,pos2, o)
    if noise:
        trace = add_noise(trace, mean=mean, std=std)  
    return trace.astype(int) 
      
def generate_pulse_from_vec(vec, is_single):
    if is_single:
        return generate_single_pulse(vec[4], vec[5], vec[6], vec[7], vec[3], 250)
    else:
        return generate_double_pulse(vec[11],vec[12], vec[13], vec[14],vec[15], vec[16],vec[17],vec[18], vec[19], 250)
    
def add_noise(trace, mean=0, std=3.38):
    vals = np.random.normal(0, 3.38,len(trace))
    for i in range(len(trace)):
        trace[i] = trace[i] + vals[i]
        
    return trace.astype(int)


