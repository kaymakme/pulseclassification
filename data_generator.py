from pulse_maker import generate_single_pulse, generate_double_pulse, add_noise
import numpy as np
from sklearn.neighbors import KernelDensity
import sys
from helper import read_fit_params, read_binary, write_binary, write_labels
import os.path
import os

# CONSTANTS
MAX_AMPLITUDE = 16000
MIN_AMPLITUDE = 60
LOWEST_POS = 30 #Only for gaussian
# double pulse: pos1,a1, k1, k2,pos2, a2,k3,k4, o
# single pulse: pos1,a1, k1, k2,o

SINGLE_PULSE_LIMITS = [[0,250],[60, 16000],[0, 1],[0, 0.1],[0, 5000]]
DOUBLE_PULSE_LIMITS = [[0,250],[60, 16000],[0, 1],[0, 0.1],
                       [0,250],[60, 16000],[0, 1],[0, 0.1],[0, 5000]]

def generate_params_gaussian(mean, cov, count, file, double_mode):
    '''
    Generate parameters for synthetic data from fitted gaussian distribution
    '''
    
    param_list = []
    
    for i in range(count):
        vals = np.random.multivariate_normal(mean, cov, 1).T
        vals = np.round_(vals, decimals=4)
        if (double_mode > -1):
            # use uniform distribution to generate pulse location params.
            dist_diff1 = double_mode * 30
            dist_diff2 = (double_mode-1) * 30
            pos1 = np.random.uniform(low=LOWEST_POS, high=250 - dist_diff1, size=1)[0]
            pos2 = np.random.uniform(low=pos1+dist_diff2, high=pos1+dist_diff1, size=1)[0]
            vals[2] = pos1
            vals[6] = pos2
        param_list.append(vals)
    return np.array(param_list)

def constraint_check(sample, limits):
    '''
    Check if the parameters are in the given range
    '''
    for i in range(len(sample)):
        if sample[i] < limits[i][0] or sample[i] > limits[i][1]:
            return False
    return True

    
def kde_sample_with_constraints(kde, is_single, length):
    '''
    Sample a param set from given kde. 
    Make sure decay and steepness params are positive  by resampling
    Amplitude and position is uniform
    returns one param set
    
    '''
    sample = kde.sample(n_samples=1)[0]
    # for the first pulse, only the amplitude is uniform.
    sample[1] = np.random.uniform(MIN_AMPLITUDE,MAX_AMPLITUDE,1)[0]
    if is_single == False:
        #uniform amplitude and position for the second pulse
        sample[5] = np.random.uniform(MIN_AMPLITUDE,MAX_AMPLITUDE,1)[0]
        sample[4] = np.random.uniform(sample[0],length,1)[0]
    
    check = False
    if is_single:
        check = constraint_check(sample, SINGLE_PULSE_LIMITS)
    else:
        check = constraint_check(sample, DOUBLE_PULSE_LIMITS)
    # to get rid of the weird parameters (negative values etc), keep drawing
    # new parameters from KDE till the conditions are met.
    while check == False:
        sample = kde.sample(n_samples=1)[0]
        sample[1] = np.random.uniform(MIN_AMPLITUDE,MAX_AMPLITUDE,1)[0]
        if is_single == False:
            sample[5] = np.random.uniform(MIN_AMPLITUDE,MAX_AMPLITUDE,1)[0]
            sample[4] = np.random.uniform(sample[0],length,1)[0]
        if is_single:
            check = constraint_check(sample, SINGLE_PULSE_LIMITS)
        else:
            check = constraint_check(sample, DOUBLE_PULSE_LIMITS)
    
    return sample
    


def generate_params_KDE_uniform_pos(kde, count, is_single, length=250):
    '''
    Generate parameters after fitting given matrix using KDE. 
    to generate double pulses, is_single=0, else 1.  
    
    Param order for double pulse params: pos1,a1, k1, k2,pos2, a2,k3,k4, o
    Param order for single pulse params: pos1,a1, k1, k2, o
    
   
    
    '''
    params = []
    for i in range(count):
        sample = kde_sample_with_constraints(kde, is_single, length)
        params.append(sample)
    params = np.array(params) 
    
    return params

def generate_double_params_KDE_uniform_pos_from_single(kde, count, length=250):
    '''
    Generate parameters after fitting given matrix using KDE. 
    to generate double pulses, is_single=0, else 1.  
    
    Param order for double pulse params: pos1,a1, k1, k2,pos2, a2,k3,k4, o
    Param order for single pulse params: pos1,a1, k1, k2, o
    '''
    params = []
    for i in range(count):
        double_sample = [0] * 9
        single_sample1 = kde_sample_with_constraints(kde, True, length)
        single_sample2 = kde_sample_with_constraints(kde, True, length)
        # position of the first pulse
        double_sample[0] = single_sample1[0]
        # for the first pulse, only the amplitude is uniform.
        double_sample[1] = np.random.uniform(MIN_AMPLITUDE,MAX_AMPLITUDE,1)[0]
        double_sample[2] = single_sample1[2]
        double_sample[3] = single_sample1[3]
        #uniform amplitude and position for the second pulse
        double_sample[5] = np.random.uniform(MIN_AMPLITUDE,MAX_AMPLITUDE,1)[0]
        double_sample[4] = np.random.uniform(double_sample[0],length,1)[0]
        double_sample[6] = single_sample2[2]
        double_sample[7] = single_sample2[3]
        double_sample[8] = single_sample2[4]
        
        params.append(double_sample)
    params = np.array(params) 
    
    return params


        
def generate_params_harcoded(count, double_mode):
    '''
    Generate parameters based on some hardcoded ranges (taken from pulse maker in c)
    '''
    OFFSET_RANGE = 10
    AMPLITUDE_RANGE = 100
    RISE_TIME_RANGE  =0.2
    FALL_TIME_RANGE = 0.01
    T0_RANGE = 50.0      
    
    o = 100
    a1 = 200
    a2 = 300
    k1 = 0.5
    k3 = 0.5
    k2 = 0.05
    k4 = 0.05
    dthigh = 200
    
    params = []
                
    for i in range(count):
        o = np.random.uniform(low=o-OFFSET_RANGE, high=o+OFFSET_RANGE, size=1)[0]
        a1 = np.random.uniform(low=a1-AMPLITUDE_RANGE, high=a1+AMPLITUDE_RANGE, size=1)[0]
        a2 = np.random.uniform(low=a2-AMPLITUDE_RANGE, high=a2+AMPLITUDE_RANGE, size=1)[0]
        k1 = np.random.uniform(low=k1-RISE_TIME_RANGE, high=k1+RISE_TIME_RANGE, size=1)[0]
        k3 = np.random.uniform(low=k3-RISE_TIME_RANGE, high=k3+RISE_TIME_RANGE, size=1)[0]
        k2 = np.random.uniform(low=k2-FALL_TIME_RANGE, high=k2+FALL_TIME_RANGE, size=1)[0]
        k4 = np.random.uniform(low=k4-FALL_TIME_RANGE, high=k4+FALL_TIME_RANGE, size=1)[0]
        pos1 =  np.random.uniform(low=250/2 - T0_RANGE, high=250/2 + T0_RANGE, size=1)[0]
        pos2 =  np.random.uniform(low=250/2 - T0_RANGE, high=250/2 + T0_RANGE, size=1)[0]
        vals = []
        if (double_mode > -1):
            dist_diff1 = double_mode * 30
            dist_diff2 = (double_mode-1) * 30
            pos1 = np.random.uniform(low=30, high=250 - dist_diff1, size=1)[0]
            pos2 = np.random.uniform(low=pos1+dist_diff2, high=pos1+dist_diff1, size=1)[0]    
            #vals = [o,0,pos1,a1,k1,k2,pos2,a2,k3,k4]
        vals = [0,0,0,pos1,a1,k1,k2,o,0,0,0,pos1,a1,k1,k2,pos2,a2,k3,k4,o]
        vals = np.round_(vals, decimals=4)
        params.append(vals)
        
    return params

def generate_traces_from_params(params, is_single, length=250, noise=True):
    # generate pulses from the parameters
    traces = np.ndarray((len(params), length))
    if is_single:
        for i,r in enumerate(params):
            traces[i,:] = generate_single_pulse(*r,n=length,noise=noise)
    else:
        for i,r in enumerate(params):
            traces[i,:] = generate_double_pulse(*r,n=length,noise=noise)
    
    return traces

def main():
    import argparse
    # TODO: Add an option to use real single pulses without generating synthetic ones
    # create parser for command-line arguments
    parser = argparse.ArgumentParser(description='Generate synthetic data to a double pulse classifier')
    parser.add_argument('single_param_file', metavar='single_param_file',
        type=str,
        help='Single pulse parameter file to fit (for KDE)')
    parser.add_argument('out_folder',
        type=str,
        help='Output folder for generated files.')  
    
    parser.add_argument('-k', '--kernel', metavar='kernel', default=['gaussian'], nargs=1,
        type=str,
        help='Kernel parameter for Kernel Density Estimation. \n \
        Available options: gaussian, tophat \n \
        (Default: gaussian)')
    parser.add_argument('-bw', '--bandwidth', metavar='bandwidth', default=[0.001], nargs=1,
        type=float,
        help='Bandwidth parameter for Kernel Density Estimation \n \
        (Default: 0.001).')
    parser.add_argument('-c', '--count', metavar='count', default=[50000], nargs=1,
        type=int,
        help='Number of traces to generate using KDE (per class) \n \
        (Default: 50000).')  
    parser.add_argument('-l', '--length', metavar='length', default=[250], nargs=1,
        type=int,
        help='Length of the traces to produce  \n \
        (Default: 250)')  
    parser.add_argument('-dpf', '--double_param_file', metavar='double_param_file', nargs=1,
        default=['not given'],
        type=str,
        help='Double pulse parameter file to fit (for KDE). \n \
        Optional if not provided single pulse parameters will be used to produce double pulses')   
    parser.add_argument('--single_trace_file', metavar='single_trace_file',nargs=1,
        default=['not given'],                
        type=str,
        help='Single pulse trace file (binary file) to use instead of generating single pulses \n \
        (since it is easy to find single pulses)')
    
    parser.add_argument('--noise', metavar='noise', default=[1], nargs=1,
        type=int,
        help='Whether to add noise to produced signals \n \
        (std and mean values should be provided to add noise)  \n \
        (Default: 1)')
    parser.add_argument('--mean', metavar='mean', default=[0], nargs=1,
        type=float,
        help='Mean value for the Gaussian Distribution used to produce noise \n \
        (Default: 0)')
    parser.add_argument('--std', metavar='std', default=[3.38], nargs=1,
        type=float,
        help='Standard deviation for the Gaussian Distribution used to produce noise \n \
        (Default: 3.38)')     
    parser.add_argument('-w_p', '--write_params', metavar='write_params', default=[0], nargs=1,
        type=int,
        help='To write generated parameters to files as well as generated signals \n \
        (Default: 0)')
    
    # parse command-line arguments
    args = parser.parse_args()
    

    k = args.kernel[0]
    bw =  args.bandwidth[0]
    length = args.length[0]
    count = args.count[0]
    noise = args.noise[0]
    
    # read parameter files to fit
    single_file_exists = os.path.exists(args.single_param_file)
    if args.single_param_file[0] == 'not given' or single_file_exists == False:
        print("[ERROR] Parameter file for single pulses is not given or does not exist!")
        sys.exit()
        
    double_file_exists = os.path.exists(args.double_param_file[0])    
    if args.double_param_file[0] == 'not given' or double_file_exists == False:
        print("[WARNING] Parameter file for double pulses is not given or does not exist! \n \
              Single pulse parameters will be used to estimate the param. distribution for double pulses!")
        
    out_exists = os.path.exists(args.out_folder) 
    
    if out_exists:
        print("[WARNING]: given folder for output files already exists")
    else:
        print("[WARNING]: given folder for output files doesnt exist, it will be created!")
        path = args.out_folder
        try:
            os.mkdir(path)
        except OSError:
            print ("[ERROR] Creation of the directory %s failed" % path)
            sys.exit()
        else:
            print ("[INFO] Successfully created the directory %s " % path)
        
    
    # generate parameters for single pulse traces
    # pos,a, k1, k2, o
    single_params_in = np.loadtxt(args.single_param_file)
    
    kde_s = KernelDensity(bandwidth = bw,kernel = k)
    kde_s.fit(single_params_in)
    # generate pulses from the parameters
    samples_s_traces = []
    if args.single_trace_file[0] == 'not given':
        parameters_single = generate_params_KDE_uniform_pos(kde_s, count, True,length) 
        for r in parameters_single:
            samples_s_traces.append(generate_single_pulse(*r,n=length,noise=False))
        samples_s_traces = np.array(samples_s_traces)
    else:
        samples_s_traces = read_binary(args.single_trace_file[0],count, False)
        
    print('[INFO] Single pulses are generated!')
    
    # generate parameters for double pulse traces
    if double_file_exists:
        #pos1,a1, k1, k2,pos2, a2,k3,k4, o
        double_params_in = np.loadtxt(args.single_param_file[0])
   
        kde_d = KernelDensity(bandwidth = bw,kernel = k)
        kde_d.fit(double_params_in)
        parameters_double = generate_params_KDE_uniform_pos(kde_d, count, False,length) 
    else:
        parameters_double = generate_double_params_KDE_uniform_pos_from_single(kde_s, count,length) 
    
    # generate pulses from the parameters
    samples_d_traces = []

    for r in parameters_double:
        samples_d_traces.append(generate_double_pulse(*r,n=length,noise=False))
    
    samples_d_traces = np.array(samples_d_traces)
    print('[INFO] Double pulses are generated!')
    
    
    

    
    X = np.concatenate((samples_d_traces, samples_s_traces), axis=0)   
    # First half is double, second half is single
    Y = [1] * count
    Y.extend([0] * count)
    Y = np.array(Y)           
    # add noise based on the param         
    if noise == 1: 
        std = args.std[0]
        mean = args.mean[0]
        for i in range(len(X)):
            X[i] = add_noise(X[i], mean=mean, std=std)
            
    name = '{}/k_{}_bw_{}_count_{}_len_{}_noise_{}'.format(args.out_folder,k,bw,count,length,noise)
    write_binary(X,name + '_X.bin')
    write_labels(Y,name + '_Y.txt')
    if args.write_params[0]:
        if args.single_trace_file[0] == 'not given':
           np.savetxt(name + '_params_single.txt',parameters_single)
        np.savetxt(name + '_params_double.txt',parameters_double)
    print('[INFO] Processs is done, traces are generated and stored')

if __name__ == "__main__":
    main()        
            
