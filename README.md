**Files to have before start**: traces.bin
Binary file where each trace starts with a 16-bit integer and the following n 16-bit integers are the values for that trace. The file contains arbitrary number of traces.

Before we do anything, we need to do the fitting to label the data and fit parameters. 


1-) **Compiling the Fitting Code and Executing it**:
The code for the fitting is located under "lmfit_code" folder. Before complinig the code, libraries provided in "load_modules.sh" should be loaded. <br>
 Makefile is provided for easy compilation. After compiling the code using the given Makefile, an executable named "main" will be generated under "bin" folder. <br>
To do the fitting, the following command could be used: <br>
```bin/main in_file out_folder start end batch_size``` <br/>
In_file: Input file name for the binary file.<br/>
Out_folder: folder to store the fitting results <br/>
Start: Starting index (indexing starts from 0) (inclusive) <br/>
End: Ending index (exclusive) <br/>
Batch_size: if Batch_size < 1, the fitting will be done in one batch and it will create one output file named {Out_folder}/results_{start}-{end}.txt.
The file will contain all of the parameters in a space seperated format. The order of the parameters: <br/>
```
      column[0] = results[i].first.iterations;
      column[1] = results[i].first.fitStatus;
      column[2] = results[i].first.chiSquare;
      column[3] = results[i].first.pulse.position;
      column[4] = results[i].first.pulse.amplitude;
      column[5] = results[i].first.pulse.steepness;
      column[6] = results[i].first.pulse.decayTime;
      column[7] = results[i].first.offset;
      column[8] = results[i].second.iterations;
      column[9] = results[i].second.fitStatus;
      column[10] = results[i].second.chiSquare;
      column[11] = results[i].second.pulses[0].position;
      column[12] = results[i].second.pulses[0].amplitude;
      column[13] = results[i].second.pulses[0].steepness;
      column[14] = results[i].second.pulses[0].decayTime;
      column[15] = results[i].second.pulses[1].position;
      column[16] = results[i].second.pulses[1].amplitude;
      column[17] = results[i].second.pulses[1].steepness;
      column[18] = results[i].second.pulses[1].decayTime;
      column[19] = results[i].second.offset; 
```





**After the fitting:**
**2-) Estimating the noise: **
```
python3 estimate_noise.py binary_trace_file output_file --count count
```
It will create a plot with the necessary information 
(Only first "count" traces will be used for the noise estimation)

**3-) Process binary and fitting data:**
```
python3 process_fitting_data.py in_fit_file in_bin_file out_folder
```
It will classify the binary data based on the fitting results and generate separate files for single and double pulses. <br/>
It will create 4 files: 2 files for parameters, 2 files for the traces <br/>
And also another 2 file to use to tune the classifier (mixed trace data and labels for it)
The mixed data will contain all of the double pulses in the given binary file and that many single pulses.

For single pulses:  pos1,a1, k1, k2,o <br/>
For double pulses: pos1,a1, k1, k2,pos2, a2,k3,k4, o <br/>
parameters will be kept in the  corresponding output files

**4-) Generating synthetic data:**
Example usage of data_generator.py (learn the distributions from the data) :
```
python3 data_generator.py single_params.txt out_folder --count 50000 --kernel gaussian --bandwidth 0.001 
--count 50000 --length 250 
--single_trace_file my_folder/single_traces.bin
 --noise 1 --mean 0 --std 3.38 
```
It will create two files named:
k_gaussian_bw_0.001_cnt_50000_len_250_noise_1_X.bin<br/>
k_gaussian_bw_0.001_cnt_50000_len_250_noise_1_Y.bin

and depending on the optional parameter -write_params, it can output param. files for the generated traces<br/>
----------------------------------------------------------------------------------------------------------------<br/>
Example usage of data_generator_v2.py (read the distributions from given files) :
```
python3 data_generator_v2.py [-h] [-l length] [-c count] [-noise noise]
                            [-mean mean] [-std std] [-w_p write_params]
                            position1 amplitude1 steepness1 decay1 position2
                            amplitude2 steepness2 decay2 offset out_file
```
Content of the files should be in the following format: <br/>
The first line contains a comma seperated list of numbers which correspond to the bin counts.  <br/>
The second list of numbers correspond to the bin values. Using this file, a histogram data is created to use as a data distribution. <br/>
5,10 ....  <br/>
1000,5000, 10000  ....<br/>

which means: P(param in [1000,5000)) =  (1/2) * P(param in [5000,10000)) <br/>
So if there are n+1 numbers in the second line, there should be n numbers in the first line <br/>

It will create a file named out_file which contains the generated double pulse traces.
<br/>

and depending on the optional parameter -w_p, it can output param. files for the generated traces<br/>

**5-) Training and tuning the classifier:**
**Training**
Just to train a classifier:
```
python3 train_classifier.py  train_X.bin train_Y.txt 
		      real_X.bin real_Y.txt
                  out_folder
		     --classifier RF
		     --tune 0
		     --num_comp 25 --n_est 200 --depth 16 --min_split 8 --min_leaf 8
```
Which will create a model(both pca transformer and classifier) in a file named: <br/>
out_folder/model_num_comp_25_n_est_200_depth_16_min_leaf_8_min_split_8.pkl

and a plot for the confusion matrix of the results after testing the model on the provided real data <br/>

**Tuning**
```
python3 train_classifier.py  train_X.bin train_Y.txt 
                  real_X.bin real_Y.txt
                  out_folder
                 --classifier RF
                 --tune 1 
                 --tune_ratio 0.2
```
Classifiers with different parameters will be trained using the provided training data and tested on the real data. <br/>
Based on the test results, a csv file will be created which contains the performance metrics of different settings for the model.<br/>
Since tune_ration is 0.2, out of all 576 different parameter settings, only int(576 * (0.2)) of them will be tested. <br/>

The file name: out_folder/tune_RF_results.csv <br/>
After deciding which parameters yield the highest performance, a new model could be trained.





**6-) Use the saved classifier:**
```
python3 classify.py my_model.pkl data.bin out_probabilities.txt
```
It will load the given model and classify the data after normalizing it.<br/>
Output file will contain 2 columns: one for P(class=single pulse) and one for P(class=double pulse) <br/>
The way it works (to call from c/c++ code): <br/>
```
model_load(model_filename) #it will create 2 glabal variables: pca and classifier
probs = normalize_and_classify(data) #using the global variables, classify the data
```
Probs is a numpy array with shape (number of traces, 2)<br/>
Useful link about converting numpy array to c/c++ array: <https://stackoverflow.com/questions/37628180/passing-a-numpy-array-to-c>
