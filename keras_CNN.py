#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 18 13:32:36 2018

@author: cagri
"""
import random
import tensorflow as tf
import time
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt 
import numpy as np
from sklearn import preprocessing
from sklearn.preprocessing import OneHotEncoder
from sklearn import metrics
import itertools
import keras
import pickle
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv1D, MaxPooling1D
from keras.models import load_model
import sys
import os.path
import os


"""
Created on Mon Oct 29 14:19:52 2018

@author: cagri
"""

matrix_1_labels = ['offset', 'chiSquare', 'pulse-0 position', 'pulse-0 amplitude',
                   'pulse-0 steepness','pulse-0 decayTime','pulse-1 position',
                   'pulse-1 amplitude','pulse-1 steepness','pulse-1 decayTime'] 

matrix_0_labels = ['offset', 'chiSquare', 'pulse-0 position', 'pulse-0 amplitude',
                   'pulse-0 steepness','pulse-0 decayTime'] 

def open_file(filename):
    f = open(filename, "r")
    return f

def read_maxmin_from_file(filename):
    list_of_list = []
    with open(filename, 'r') as fptr:   
        for line in fptr:
            my_list = []
            line = line.strip()
            line= line.replace("[", "")
            line= line.replace("]", "")
            line= line.replace(" ", "")
            arr = line.split(',')

            for val in arr:
                my_list.append(float(val))
            list_of_list.append(my_list)
    return  list_of_list

    
        

def read_fit_info(file_name):

    fptr = open_file(file_name)
    ctr = 0
    labels = []
    matrix_1 = []
    matrix_0 = []
    results_w_dict = []

    min_list_0 = []
    max_list_0 = []

    min_list_1 = []
    max_list_1 = []
    for line in fptr:
             
        line = line.strip().split()
            
        
        fit1 = (float(line[0]),float(line[1]),float(line[2]),float(line[3]),float(line[4]),float(line[5]),float(line[6]),float(line[7]))
        fit2 = (float(line[8]),float(line[9]),float(line[10]),float(line[11]),float(line[12]),float(line[13]),float(line[14]),float(line[15]),float(line[16]),float(line[17]),float(line[18]),float(line[19]))
        
        fit1_pulse_dict = {'position' : fit1[3],
                     'amplitude' : fit1[4],
                     'steepness' : fit1[5],
                     'decayTime' : fit1[6]}
        
        fit2_pulse1_dict = {'position' : fit2[3],
                     'amplitude' : fit2[4],
                     'steepness' : fit2[5],
                     'decayTime' : fit2[6]}
        
        fit2_pulse2_dict = {'position' : fit2[7],
                     'amplitude' : fit2[8],
                     'steepness' : fit2[9],
                     'decayTime' : fit2[10]}
        
        fit1_dict = {'iterations' : fit1[0], 
                     'fitStatus' : fit1[1],
                     'chiSquare' : fit1[2],
                     'offset' : fit1[7],
                     'pulse' : fit1_pulse_dict}
        
        fit2_dict = {'iterations' : fit2[0], 
                     'fitStatus' : fit2[1],
                     'chiSquare' : fit2[2],
                     'offset' : fit2[11],
                     'pulses' : (fit2_pulse1_dict, fit2_pulse2_dict)}
        
        if fit2[2] == 0:
            print(ctr)
        
        if fit1[2]/fit2[2] > 10 and fit2[3] > 0 and fit2[3] < 250 and fit2[7] > 0 and fit2[7] < 250 and fit2[5] > 0 and fit2[6] > 0 and fit2[9] > 0 and fit2[10] > 0 and fit2[4]> 0 and fit2[8] > 0:
            labels.append(1)
            row = {'fit1':fit1_dict, 'fit2':fit2_dict, 'label':1, 'index':ctr}
            inner_row = []
            inner_row.append(row['fit2']['offset'])
            inner_row.append(row['fit2']['chiSquare'])
            inner_row.append(row['fit2']['pulses'][0]['position'])
            inner_row.append(row['fit2']['pulses'][0]['amplitude'])
            inner_row.append(row['fit2']['pulses'][0]['steepness'])
            inner_row.append(row['fit2']['pulses'][0]['decayTime'])
            inner_row.append(row['fit2']['pulses'][1]['position'])
            inner_row.append(row['fit2']['pulses'][1]['amplitude'])
            inner_row.append(row['fit2']['pulses'][1]['steepness'])
            inner_row.append(row['fit2']['pulses'][1]['decayTime'])
            
            if len(min_list_1) == 0:
                import copy
                min_list_1 = copy.deepcopy(inner_row)
                max_list_1 = copy.deepcopy(inner_row)
            else:
                for i in range(len(min_list_1)):
                    if min_list_1[i] > inner_row[i]:
                        min_list_1[i] = inner_row[i]
                    if max_list_1[i] < inner_row[i]:
                        max_list_1[i] = inner_row[i]

            matrix_1.append(inner_row)
        else:
            labels.append(0)
            row = {'fit1':fit1_dict, 'fit2':fit2_dict, 'label':0, 'index':ctr}
            inner_row = []
            inner_row.append(row['fit1']['offset'])
            inner_row.append(row['fit1']['chiSquare'])
            inner_row.append(row['fit1']['pulse']['position'])
            inner_row.append(row['fit1']['pulse']['amplitude'])
            inner_row.append(row['fit1']['pulse']['steepness'])
            inner_row.append(row['fit1']['pulse']['decayTime'])

            if len(min_list_0) == 0:
                import copy
                min_list_0 = copy.deepcopy(inner_row)
                max_list_0 = copy.deepcopy(inner_row)
            else:
                for i in range(len(min_list_0)):
                    if min_list_0[i] > inner_row[i]:
                        min_list_0[i] = inner_row[i]
                    if max_list_0[i] < inner_row[i]:
                        max_list_0[i] = inner_row[i]

            matrix_0.append(inner_row)
        ctr = ctr+1

    return ((matrix_0,matrix_1),labels)


def open_file(filename):
    f = open(filename, "r")
    return f

def read2list(fileptr, type_):
    my_str = fileptr.readline().strip();
    num_list = []
    for s in my_str.split():
        num_list.append(type_(s))
    return num_list;

def plot(trace_list, title):
    plt.title(title)
    plt.plot(trace_list) 

def read_traces(ntraces, folder):
    act_folder = "./" + folder + "/"
    traces = []
    for i in range(ntraces):
        trace_no = i;
        trace_file1 = "trace_" + str(trace_no) + ".txt"
        fileptr = open_file(act_folder + trace_file1)
        trace_list1 = read2list(fileptr, float)
        traces.append(trace_list1)
    return traces


def get_random_batch(x, y, size):  
    index_list = random.sample(range(0, y.shape[0]), size)
    return (x[index_list,:], y[index_list,:])


def get_single_and_double_pulses(singles_doubles, filename):
    fptr = open(filename, 'r')
    last_index = 0
    signals = []
    for row in singles_doubles:
        index = row[0]
        fptr.seek(504 * index)
        sig = read_binary(fptr, 1,False)[0][0]
        signals.append(sig)
        
    return signals
       
def read_binary(fptr, sample_count, normalize):       
    samples = []
    i = 0
    all_read = False
    while sample_count == -1 or i < sample_count:
        size = np.fromfile(fptr, dtype='uint32', count=(1))
        if not size.size: 
            all_read = True
            break
        if i == 0:
            size_first = size[0]
        one_sample = np.fromfile(fptr, dtype='uint16', count=(size[0]))
        samples.append(one_sample)
        i = i+1              
 
    samples = np.array(samples, dtype='uint16')
    
    if normalize:
        samples = preprocessing.normalize(samples) 
        '''
        for i,s in enumerate(samples):
            s = cut_signal(s,multip)
            samples[i] = s
        samples = preprocessing.normalize(samples) 
        '''
    samples = samples.reshape(-1, size_first, 1)
    return (samples, all_read)

def write_binary(signals, filename):
    with open(filename, 'bw') as f:
        for sig in signals: 
            size = np.uint32(len(sig))
            size_b = struct.pack('I', size)
            f.write(size_b)
            for num in sig:
                bin_num = struct.pack('H', num[0])
                f.write(bin_num)

def predictions_to_labels(predictions):
    label_p = np.argmax(predictions, axis = 1) 
    return label_p

def CNN_keras(x_train, y_train, x_valid, y_valid, num_classes, epoch, batch_size, layer_count, layer_sizes,kernel_sizes, fully_size, drop):
    
    input_shape = x_train[0].shape
    model = Sequential()
    model.add(Conv1D(layer_sizes[0], kernel_size=kernel_sizes[0],
                 activation='relu',
                 input_shape=input_shape))
    model.add(MaxPooling1D(pool_size=2))
    for i in range(1,layer_count):        
        model.add(Conv1D(layer_sizes[i], kernel_size=kernel_sizes[i],
                         activation='relu'))    
        model.add(MaxPooling1D(pool_size=2))       
        
    model.add(Flatten())
    model.add(Dense(fully_size, activation='relu'))
    model.add(Dropout(drop))
    model.add(Dense(num_classes, activation='softmax'))
    
    model.compile(loss=keras.losses.mean_squared_error,
              optimizer=keras.optimizers.Adadelta(lr=1.0, rho=0.95, epsilon=1e-08, decay=0.0),
              metrics=['accuracy'])
    
    hist = model.fit(x_train, y_train,
              batch_size=batch_size,
              epochs=epoch,
              verbose=1,validation_data=(x_valid, y_valid))
    
    return (hist,model)

def fill_file_with_random_params(mean, cov, count, file, double_mode):
    fptr = open(file, 'w')  
    
    for i in range(count):
        vals = np.random.multivariate_normal(mean, cov/2, 1).T
        vals = np.round_(vals, decimals=4)
        if (double_mode > -1):
            dist_diff1 = double_mode * 30
            dist_diff2 = (double_mode-1) * 30
            pos1 = np.random.uniform(low=30, high=250 - dist_diff1, size=1)[0]
            pos2 = np.random.uniform(low=pos1+dist_diff2, high=pos1+dist_diff1, size=1)[0]
            vals[2] = pos1
            vals[6] = pos2
        line = ''
        for val in vals:
            line = line + ' ' + str(val[0])
        if i == count - 1:
            print(line.strip(), file=fptr, end='')
        else:
            print(line.strip(), file=fptr)
    fptr.close()
    

def read_datasets(filenames, labels, normalize):
    X = []
    Y = []
    for i,filename in enumerate(filenames):
        fptr = open_file(filename)
        samples = read_binary(fptr, -1, normalize)[0]
        sub_labels = [labels[i]] * len(samples)
        Y.extend(sub_labels)
        X.append(samples)
        fptr.close()
   
    X_arr = X[0]
    
    for x in X[1:]:
        X_arr = np.concatenate((X_arr, x), axis = 0)
        
    Y_arr = np.array(Y)
    onehot_encoder = OneHotEncoder(sparse=False)
    integer_encoded = Y_arr.reshape(len(Y_arr), 1)
    onehot_encoded = onehot_encoder.fit_transform(integer_encoded)
        
    return (X_arr, onehot_encoded)

def plot_confusion_matrix(unique_name,cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)
    plt.figure(unique_name)
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')  
    plt.savefig( '/mnt/home/kaymakme/CNN_testing/results/info/' + unique_name + '_confusion.png')

# find the cut length of a trace
def find_cut_length(trace):
    max_v = max(trace)
    count = 0
    for v in trace:
        if v == max_v:
            count = count + 1
            
    return count

def find_all_cut_lengths(traces):
    counts = []
    
    for trace in traces:
        count = find_cut_length(trace)
        counts.append(count)
        
    return counts   

def main():
    normalize = True
    num_classes = int(sys.argv[1]) # not used
    normalize =  bool(sys.argv[2])
    layer_count = int(sys.argv[3])
    layer_sizes = []
    kernel_sizes = []
    unique_name = str(layer_count) + '_'

    for i in range(layer_count):      
       layer_sizes.append(int(sys.argv[3 + i + 1])) 
       unique_name = unique_name + '_' + str(layer_sizes[i])
       
    for i in range(layer_count):      
       kernel_sizes.append(int(sys.argv[layer_count + 3 + i + 1])) 
       unique_name = unique_name + '_' + str(kernel_sizes[i])
       
    fully_con_size = int(sys.argv[3 + layer_count*2 + 1])
    unique_name = unique_name + '_' + str(fully_con_size)
    k = str(sys.argv[-4])
    bw =  float(sys.argv[-3])
    noise = int(sys.argv[-2])
    epoch = int(sys.argv[-1])
    '''
    train_folder = '/mnt/home/kaymakme/CNN_testing/dataset_transformed_CNN_cut/test/'
    test_folder = '/mnt/home/kaymakme/CNN_testing/dataset_transformed_CNN_cut/train/' 

    if len(sys.argv) > 3 + layer_count*2 + 1:
        train_folder = sys.argv[-2]
        test_folder  = sys.argv[-1]
    print(train_folder)  
    print(test_folder)
    '''
    if noise==1:
        noise = '_w_noise.npy'
    else:
        noise = '_wo_noise.npy'

    name_first ='/mnt/home/kaymakme/tree_methods/KDE_generation/KDE_DATASETS_60_TRAIN_SPLIT/' + k + '_' + str(bw) + noise
    print('STARTING,', name_first)
    X = np.load(name_first)
    print(X.shape)
    X = preprocessing.normalize(X)

    X_train = np.concatenate((X[:200000,:], X[200000:400000,:]), axis=0).reshape(-1,250,1)
    X_test = np.concatenate((X[100000:200000,:], X[300000:400000,:]), axis=0).reshape(-1,250,1)
    Y_train = [1] * 200000
    Y_train.extend([0] * 200000)
    Y_train = np.array(Y_train)
    Y_test = [1] * 100000
    Y_test.extend([0] * 100000)
    Y_test = np.array(Y_test)         
    unique_name =  name_first.split('/')[-1] + unique_name

    onehot_encoder = OneHotEncoder(sparse=False)
    integer_encoded = Y_train.reshape(len(Y_train), 1)
    Y_train = onehot_encoder.fit_transform(integer_encoded)

    onehot_encoder = OneHotEncoder(sparse=False)
    integer_encoded = Y_test.reshape(len(Y_test), 1)
    Y_test = onehot_encoder.fit_transform(integer_encoded)    
    
    uncut_XY_2_4M = np.load('/mnt/home/kaymakme/tree_methods/uncut_XY_2_4M.npy') # normalized
    test_X_real = uncut_XY_2_4M[:, :250].reshape(-1,250)
    test_X_real = preprocessing.normalize(test_X_real).reshape(-1,250,1)
    test_Y_real = uncut_XY_2_4M[:, 250] 

    onehot_encoder = OneHotEncoder(sparse=False)
    integer_encoded = test_Y_real.reshape(len(test_Y_real), 1)
    test_Y_real = onehot_encoder.fit_transform(integer_encoded)   

    print("Unique name:", unique_name)
    '''
    all_data_test = np.load(test_np_file)
    all_data_train = np.load(train_np_file)
    X_test = all_data_test[:, :250]
    Y_test = all_data_test[:, 250]
    X_test = X_test.reshape(-1, 250, 1)
    X_train = all_data_train[:, :250]
    Y_train = all_data_train[:, 250]
    X_train = X_train.reshape(-1, 250, 1)

    for i in range(len(Y_train)):
        if num_classes == 2:
            if Y_train[i] > 1:
                Y_train[i] = 1
        elif num_classes == 3:
            if Y_train[i] == 2:
                Y_train[i] = 1
            elif Y_train[i] in [3,4]:
                Y_train[i] = 2

    for i in range(len(Y_test)):
        if num_classes == 2:
            if Y_test[i] > 1:
                Y_test[i] = 1
        elif num_classes == 3:
            if Y_test[i] == 2:
                Y_test[i] = 1
            elif Y_test[i] in [3,4]:
                Y_test[i] = 2
    
    onehot_encoder = OneHotEncoder(sparse=False)
    integer_encoded = Y_train.reshape(len(Y_test), 1)
    Y_test = onehot_encoder.fit_transform(integer_encoded)

    onehot_encoder = OneHotEncoder(sparse=False)
    integer_encoded = Y_train.reshape(len(Y_train), 1)
    Y_train_ = onehot_encoder.fit_transform(integer_encoded)
    '''

    (X_valid, Y_valid) = get_random_batch(test_X_real, test_Y_real, 40000)
    
    
   
    model_fname = '/mnt/home/kaymakme/tree_methods/CNN_models/' + unique_name
    if (os.path.isfile(model_fname) == False):
        (hist,model) = CNN_keras(X_train, Y_train, X_valid, Y_valid, 2, epoch, 256, layer_count, layer_sizes,kernel_sizes, fully_con_size, 0.5)
        with open('/mnt/home/kaymakme/tree_methods/CNN_models/' + unique_name + '.hist', 'wb') as file_pi:
            pickle.dump(hist.history, file_pi)
    else:
        model = load_model(model_fname)
        (hist,model) = CNN_keras(X_train, Y_train, X_valid, Y_valid, 2, epoch, 256, layer_count, layer_sizes,kernel_sizes, fully_con_size, 0.5)
    
    model.save(model_fname)
    model.save_weights(model_fname + '.h5')
    with open(model_fname + '.json', 'w') as f:
        f.write(model.to_json())
    print("model Saved to disk") 
    
    preds = model.predict(X_test, batch_size=500, verbose=1)
    labs_p = predictions_to_labels(preds)
    labs_r = predictions_to_labels(Y_test)
    cm = metrics.confusion_matrix(labs_r, labs_p)
    print(cm)
    comp = labs_p == labs_r
    acc = sum(comp)/len(comp)
    with open('/mnt/home/kaymakme/tree_methods/CNN_models/info/' + unique_name + '.test', 'wb') as f:
        pickle.dump(cm, f)  

    '''
    fptr = open_file('/mnt/home/kaymakme/CNN_testing/first2m.bin')
    ((matrix_0_2m,matrix_1_2m),labels_2m) = read_fit_info('/mnt/home/kaymakme/CNN_testing/fit_2m.txt')
    first_2m_samples = read_binary(fptr, -1, normalize)[0]
    labs_r = np.array(labels_2m)
    '''

    preds = model.predict(test_X_real, batch_size=500, verbose=1)
    labs_p = predictions_to_labels(preds)
    labs_r = predictions_to_labels(test_Y_real)

    cm = metrics.confusion_matrix(labs_r,labs_p)
    print(cm)
    with open('/mnt/home/kaymakme/tree_methods/CNN_models/info/' + unique_name + '.test_real', 'wb') as f:
        pickle.dump(cm, f)  

    # serialize model to JSON

    # serialize weights to HDF5
    

if __name__ == "__main__":
    main()


#######################################                
#######################################

#######################################
#######################################

'''
(hist,model) = CNN_keras(train_x, onehot_encoded, 3)
with open('/mnt/home/kaymakme/CNN_testing/trainHistoryDict', 'wb') as file_pi:
        pickle.dump(hist.history, file_pi)

'''