#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr  7 17:46:44 2019

@author: cagri
"""
from sklearn import metrics
from sklearn.decomposition import PCA
from sklearn.svm import SVC
from sklearn.metrics import classification_report
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn import preprocessing
import sys
import numpy as np
import joblib
from helper import read_binary


def load_model(model_name):
    '''
    Load the saved model(pca + classifier) from given file
    the function doesnt return anything but initializes the global variables
    '''
    global classifier, pca
    pca, classifier = joblib.load(model_name)

def normalize_and_classify(data):
    '''
    Takes numpy array of traces as parameter(data.shape = (trace_count, trace_length))
    Normizes and transform the data using pca.
    Then does the classification and return the probabilities
    the shape of prob array is (trace_count, 2) 
    where the first column is probility of given trace is single
    
    '''
    length = data.shape[1]
    # get rid of the offset 
    data = remove_offset_value(data)
    # normalize the traces
    data = preprocessing.normalize(data.reshape(-1,length))  

    # transform the data
    data = pca.transform(data)
    # classify
    probs = classifier.predict_proba(data)   
    
    return probs


def remove_offset_value(traces):
    '''
    Get rid of the offset value by subtracting the min
    '''
    min_vals = np.min(traces, axis = 1) .reshape(-1,1)
    traces = traces - min_vals
    return traces

    
def create_grid(y_pred_forest, diffs, rel_amps,num_pos,num_rel_amp):
    '''
    Create a grid where x axis is the difference between pulses and y axis
    is the relative amplitude difference between the pulses
    
    Useful to analyze a performance of classifier on double pulses
    '''
    max_diff = 150
    min_diff = 0
    
    max_relative_amp = 1
    min_relative_amp = 0
    
    my_grid = []
    counts = np.zeros(shape=(num_pos,num_rel_amp))
    for i in range(num_pos):
        my_grid.append([])
        for j in range(num_rel_amp):
            my_grid[i].append([])
    
    for i in range(len(y_pred_forest)):
        index1 = find_index(diffs[i], min_diff, max_diff, num_pos)
        index2 = find_index(rel_amps[i], min_relative_amp, max_relative_amp, num_rel_amp)
        my_grid[index1][index2].append(y_pred_forest[i])
    # calculate accuracies
    for i in range(num_pos):
        for j in range(num_rel_amp):
            if len(my_grid[i][j]) > 0:
                counts[i][j] = len(my_grid[i][j])
                my_grid[i][j] = sum(my_grid[i][j]) / len(my_grid[i][j])
            else:
                counts[i][j] = 0
                my_grid[i][j] = 0
    return my_grid, counts
        
def find_index(value,min_val, max_val, num_bins):
    '''
    helper for create_grid
    
    '''
    if value >= max_val:
        return num_bins - 1
    if value <= min_val:
        return 0
    step = (max_val - min_val) / num_bins
    index = int((value - min_val) / step)
    if index >= num_bins:
        index = num_bins - 1
        
    return index

           
    
def main():
    import argparse
    
    # create parser for command-line arguments
    parser = argparse.ArgumentParser(description='Classify the data and return the labels')
    parser.add_argument('model_name', metavar='filename',
        type=str,
        help='Model file to load')
    
    parser.add_argument('data_file', metavar='filename',
        type=str,
        help='traces to classify (raw binary data)') 
      
    parser.add_argument('out_file', metavar='filename',
        type=str,
        help='Output file for the results.')       
 
   

    # parse command-line arguments
    args = parser.parse_args()
    # read the model
    load_model(args.model_name)
        
    # read the data
    data = read_binary(args.data_file, -1, False)


    probs = normalize_and_classify(data)
    
    np.savetxt(args.out_file,probs)
    
                    

if __name__ == "__main__":
    main() 