import numpy as np
import sys


def find_pulse_starting_point(trace, distance = 10, increase_perc=0.03):
    '''
    Find the starting point of the pulse
    If |trace[i+distance] - trace[i]| > trace[i] * increase_perc
        return i
    else
        return length of the trace 
        
    Input: A single trace
    Output: the starting position
    
    
    '''
    for i in range(len(trace)-distance):
        start = int(trace[i])
        end = int(trace[i+distance])
        diff = abs(end - start)
        if diff > start * increase_perc:
            return i
    return len(trace) # indicates there is no pulse


def calculate_difference(trace):
    '''
    Find the starting position of the pulse
    Then analyze [0, limit] segment to detect the noise
    3.1 Synthetic Data Generation: Noise Estimation
    
    Input: A single trace 
    Output: a list of values (difference from the mean)
    
    
    '''
    limit = find_pulse_starting_point(trace)
    # if the startin position > 30, set it to 30 since that is enough to
    # detect the noise
    if limit > 30:
        limit = 30
    
    diff_mean = []
    if limit != 0:
        mean = sum(trace[:limit])/limit
        
        for v in trace[:limit]:
            diff_mean.append(v - mean) 
    return diff_mean

def detect_noise(traces, return_dif_array=False):
    '''
    By iterating through the list of traces, estimate the noise and its params
    
    Input: traces: A list of traces
           return_dif_array: boolean, if it is True, return the difference array
           (only needed to double check the estimation and plotting)
           
    Output:
        mean and std of the noise distribution (Additive Gaussian)
        if return_dif_array is true, returns mean, std, diff_arr
    
    '''
    diff_list = []
    for i,trace in enumerate(traces):
        diff_list.extend(calculate_difference(trace))  
    diff_arr = np.array(diff_list)
    
    # to get rid of weird extreme values
    ext_ends = len(diff_arr) * 1/10000
    ext_ends = int(ext_ends)
    if ext_ends == 0:
        ext_ends = 1
    diff_arr = sorted(diff_arr)[ext_ends:-ext_ends]
    
    # calculate mean, var and sigma
    mean = np.mean(diff_arr)
    variance = np.var(diff_arr)
    sigma = np.sqrt(variance)
    
    if return_dif_array:
        return mean, sigma,diff_arr
    else:
        return mean, sigma
    
def plot_info_for_noise(mean, std, diff_arr, out_file):
    '''
    Plot the histogram of the difference array (calculated with detect_noise)
    and fitted gaussian distribution.
    
    Input: traces: A list of traces          
    '''
    from matplotlib import pyplot as plt
    from scipy.stats import norm
    
    x = np.linspace(-20, 20, 100)
    plt.rcParams.update({'font.size': 13})
    plt.title('Noise Estimation: mean={}, std={}'.format(mean,std))
    l1 = plt.hist(diff_arr,range=(-20,20),bins=100,density=True, label='Histogram of the Noise')
    l2, = plt.plot(x, norm.pdf(x, mean, std), label="Fitted Gaussian Distribution")
    
    
    plt.legend()
    plt.savefig(out_file)
    

def find_cut_length(trace):
    '''          
    find the cut length of a given trace
    
    '''
    max_v = max(trace)
    count = 0
    for v in trace:
        if v == max_v:
            count = count + 1
            
    return count

def find_all_cut_lengths(traces):
    '''          
    find cut lengths of given traces
    
    '''
    counts = []
    
    for trace in traces:
        count = find_cut_length(trace)
        counts.append(count)
        
    return counts



def random_cut(X, Y): 
    '''
    Add random cuts to the given training data
    '''
    for i in range(len(X)):
        random1 = np.random.uniform(low=1, high=10,size=1)[0]
        random2 = np.random.uniform(low=15, high=34,size=1)[0]
        is_double = ((Y[i].argmax(axis=0)) == 1)
        
        if (is_double and random1 > 3) or (is_double == False and random1 > 8):
            cut_length = int(random2)
            sorted_inds = np.argsort(-X[i])
            min_of_maxs = X[i][sorted_inds[cut_length - 1]]
            for j in sorted_inds[:cut_length]:
                X[i][j] = min_of_maxs

def kde_cut(X, kde):
    '''
    Get the cut length from estimated distribution of cut lengths
    '''
    for i in range(len(X)):
        val = kde.sample(n_samples=1)[0]
        if val > 0:
            cut_length = val
            sorted_inds = np.argsort(-X[i])
            min_of_maxs = X[i][sorted_inds[cut_length - 1]]
            for i in sorted_inds[:cut_length]:
                X[i] = min_of_maxs
    return X
                
    
                    
def open_file(filename):
    f = open(filename, "r")
    return f


def plot(trace_list, title):
    '''
    plot trace
    '''
    from matplotlib import pyplot as plt
    plt.title(title)
    plt.plot(trace_list)
    


    

def read_fit_params(file_name):
    '''
    Read parameters from a given file
    The expected file format: 
      arr[0] = labels[i].first.iterations;
      arr[1] = labels[i].first.fitStatus;
      arr[2] = labels[i].first.chiSquare;
      arr[3] = labels[i].first.pulse.position;
      arr[4] = labels[i].first.pulse.amplitude;
      arr[5] = labels[i].first.pulse.steepness;
      arr[6] = labels[i].first.pulse.decayTime;
      arr[7] = labels[i].first.offset;
      arr[8] = labels[i].second.iterations;
      arr[9] = labels[i].second.fitStatus;
      arr[10] = labels[i].second.chiSquare;
      arr[11] = labels[i].second.pulses[0].position;
      arr[12] = labels[i].second.pulses[0].amplitude;
      arr[13] = labels[i].second.pulses[0].steepness;
      arr[14] = labels[i].second.pulses[0].decayTime;
      arr[15] = labels[i].second.pulses[1].position;
      arr[16] = labels[i].second.pulses[1].amplitude;
      arr[17] = labels[i].second.pulses[1].steepness;
      arr[18] = labels[i].second.pulses[1].decayTime;
      arr[19] = labels[i].second.offset; 
      
      file << arr[j] << " "  // space seperated
    '''

    fptr = open_file(file_name)
    ctr = 0
    matrix = []
    #results_w_dict = []
    
    for line in fptr:
             
        line = line.strip().split()
        if  len(line) != 20:
            print(ctr, line)
            continue

        fit1 = [float(line[0]),float(line[1]),float(line[2]),float(line[3]),float(line[4]),float(line[5]),float(line[6]),float(line[7])]
        fit2 = [float(line[8]),float(line[9]),float(line[10]),float(line[11]),float(line[12]),float(line[13]),float(line[14]),float(line[15]),float(line[16]),float(line[17]),float(line[18]),float(line[19])]
        '''
        fit1_pulse_dict = {'position' : fit1[3],
                     'amplitude' : fit1[4],
                     'steepness' : fit1[5],
                     'decayTime' : fit1[6]}
        
        fit2_pulse1_dict = {'position' : fit2[3],
                     'amplitude' : fit2[4],
                     'steepness' : fit2[5],
                     'decayTime' : fit2[6]}
        
        fit2_pulse2_dict = {'position' : fit2[7],
                     'amplitude' : fit2[8],
                     'steepness' : fit2[9],
                     'decayTime' : fit2[10]}
        
        fit1_dict = {'iterations' : fit1[0], 
                     'fitStatus' : fit1[1],
                     'chiSquare' : fit1[2],
                     'offset' : fit1[7],
                     'pulse' : fit1_pulse_dict}
        
        fit2_dict = {'iterations' : fit2[0], 
                     'fitStatus' : fit2[1],
                     'chiSquare' : fit2[2],
                     'offset' : fit2[11],
                     'pulses' : (fit2_pulse1_dict, fit2_pulse2_dict)}
        '''
        
            
        #row = {'fit1':fit1_dict, 'fit2':fit2_dict,'index':ctr}
        #results_w_dict.append(row)
        row_m = []
        row_m.extend(fit1)
        row_m.extend(fit2)
        matrix.append(row_m)
        #if fit1[2]/fit2[2] > 10 and fit2[3] > 0 and fit2[3] < 250 and fit2[7] > 0 and fit2[7] < 250 and fit2[5] > 0 and fit2[6] > 0 and fit2[9] > 0 and fit2[10] > 0 and fit2[4]> 0 and fit2[8] > 0:
           # labels.append(1)
            
                    

        ctr = ctr+1

    return np.array(matrix)

def classify_with_chisquare(matrix):
    labels = []
    for row in matrix:
        if row[2]/row[10] > 10:
            # if there are negative values, assume single pulse
            if len(np.where(row[11:] < 0)[0]) > 0:
                labels.append(0)
            else:
                labels.append(1)
        else:
            labels.append(0)
            
    return labels


def chisquare_analysis(list_of_dict):
    chi_squares_1 = []
    chi_squares_0 = []
    
    for row in list_of_dict:
        if row['label'] == 0:
            chi_squares_0.append(row['fit1']['chiSquare']/row['fit2']['chiSquare'])
        else:
            chi_squares_1.append(row['fit1']['chiSquare']/row['fit2']['chiSquare'])
    
    return (np.array(chi_squares_0),np.array(chi_squares_1))

def chisquare_analyze_plot():
    from matplotlib import pyplot as plt
    ((mat0,mat1,results_w_dict),labels) = read_fit_info('./fit_2m.txt')
    
    d_cuts = find_all_cut_lengths(doubles_real_2m)
    s_cuts = find_all_cut_lengths(singles_real_2m) 
    cut_chi_0 = []
    cut_chi_1 = []
    (chi_squares_1,chi_squares_1) = chisquare_analysis(results_w_dict)
    for i in range(len(s_cuts)):
        if s_cuts[i] > 3:
            cut_chi_0.append(chi_squares_0[i])
    for i in range(len(d_cuts)):
        if d_cuts[i] > 3:
            cut_chi_1.append(chi_squares_1[i])
            
    plt.hist((cut_chi_0,cut_chi_1), bins=100,range=(0,16), color=['red','green'], label=['single pulse','double pulse'])
    plt.xlabel('(chisquare for single pulse)/(chisquare for double pulse)')
    plt.legend(prop={'size': 10})
    plt.ylabel('frequency')
    plt.title('Analysis of the Cut Signals in the First 2M samples')
    

def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          out_file='confusion.png'):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    import itertools
    from matplotlib import pyplot as plt
    
    cmap = plt.cm.Blues
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]


    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label') 
    plt.savefig(out_file)
 
      
    
    
def read_binary(filename, count, normalize):    
    '''
    read a given binary file
    if count = -1, read everything
    otherwise read the first count traces
    
    '''
    samples = []
    i = 0
    all_read = False
    with open(filename, 'rb') as fptr:
        while count == -1 or i < count:
            size = np.fromfile(fptr, dtype='uint32', count=(1))
            if not size.size: 
                all_read = True
                break
            # get the length of traces
            if i == 0:
                size_first = size[0]
            one_sample = np.fromfile(fptr, dtype='uint16', count=(size[0]))
            samples.append(one_sample)
            i = i+1              
 
    samples = np.array(samples, dtype='uint16')
    
    if normalize:
        from sklearn import preprocessing
        samples = preprocessing.normalize(samples) 
    samples = samples.reshape(-1, size_first)
    return samples

def write_binary(signals, filename):
    import struct
    with open(filename, 'wb') as f:
        for sig in signals: 
            size = np.uint32(len(sig))
            size_b = struct.pack('I', size)
            f.write(size_b)
            for num in sig:
                bin_num = struct.pack('H', int(num))
                f.write(bin_num)
                
def write_labels(labels, filename):
    '''
    write labels to a file
    '''
    with open(filename, 'w') as f:
        for i,l in enumerate(labels):
            if i == len(labels) - 1:
                f.write("%i" % l)
            else:
                f.write("%i\n" % l)
                
def read_labels(filename):
    '''
    read labels
    '''
    with open(filename, 'r') as f:
        labels = f.readlines()
        labels = [int(l) for l in labels]
    return np.array(labels)
        

def predictions_to_labels(predictions):
    label_p = np.argmax(predictions, axis = 1) 
    return label_p
