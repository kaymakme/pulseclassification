#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr  7 17:46:44 2019

@author: cagri
"""
import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning)
from sklearn import metrics
from sklearn.decomposition import PCA
from sklearn.svm import SVC
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import classification_report
from sklearn.svm import SVC
from sklearn import tree
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn import preprocessing
import sys
import numpy as np
import joblib
from helper import read_binary, read_labels,plot_confusion_matrix
import os

def SVM_classifier(train_X, train_Y, kernel, gamma, C):
    clf = SVC(kernel = kernel, gamma=gamma, C=C)
    clf.fit(train_X, train_Y)
    return clf

def decision_tree(train_X, train_Y,min_samples_leaf,min_samples_split):
    clf = tree.DecisionTreeClassifier(min_samples_leaf=min_samples_leaf,
                                      min_samples_split=min_samples_split)
    clf.fit(train_X, train_Y) 
    return clf

def random_forest(train_X, train_Y,n_estimators,max_depth,min_samples_leaf,min_samples_split):
    clf = RandomForestClassifier(n_estimators=n_estimators, 
                                 max_depth=max_depth, 
                                 min_samples_leaf=min_samples_leaf,
                                 min_samples_split=min_samples_split)
    clf.fit(train_X, train_Y) 
    return clf

def tune_random_forest(train_X_tr, train_Y,valid_X_tr, valid_Y, tune_ratio=0.2):
    '''
    Tune up random forest classifier
    Do a random search in a given parameter list
    if try_ratio=1, will do a grid search
    Returns a lists of different metrics for each classifier
    '''
    import random
    
    all_results = []
    hyperparam_all_pairs_list = []
    # create a list which holds parameter sets
    for num_comp in [5,10,25,50]:
        for n_est in [50, 100, 200, 400]:
            for depth in [8,16,32,64]:
                for split in [2, 8, 32]:
                    for leaf in [1, 8, 32]:
                        hyperparam_all_pairs_list.append((num_comp,n_est,depth,split,leaf))
    # select parameters to try from the list
    random.shuffle(hyperparam_all_pairs_list)
    count = int(len(hyperparam_all_pairs_list) * tune_ratio)
    selected_ones = hyperparam_all_pairs_list[:count] 
    print("[INFO] Out of {} parameter sets, randomly selected {} sets will be tested and the results will be reported".format(len(hyperparam_all_pairs_list), count))
    print("[INFO] {:>10} {:>10} {:>10} {:>10} {:>10} {:>10} {:>10} {:>10} {:>10}".format('num_comp','n_est','depth','leaf','split','recall','precision', 'accuracy','f1'))

    for num_comp,n_est,depth,split,leaf in selected_ones:      
        clf = random_forest(train_X_tr[:, :num_comp], train_Y,n_est,depth,leaf,split)
        y_pred = clf.predict(valid_X_tr[:, :num_comp])
        cm = metrics.confusion_matrix(valid_Y, y_pred)
        (recall,precision, acc,f1) = get_metrics(cm)
        print("[INFO] {:>10} {:>10} {:>10} {:>10} {:>10} {:>10} {:>10} {:>10} {:>10}".format(num_comp,n_est,depth,leaf,split,round(recall,2),round(precision,2), round(acc,2),round(f1,2)))
        all_results.append((num_comp,n_est,depth,leaf,split,recall,precision, acc,f1))
    return all_results

def get_metrics(cm):
    '''
    from a given confusion matrix with 2 classes
    calculate recall, precision, accuracy and f1 scores
    '''
    recall = cm[1][1]/(cm[1][0] + cm[1][1])
    precision = cm[1][1]/(cm[1][1] + cm[0][1])
    acc = (cm[1][1] + cm[0][0])/sum(sum(cm))
    f1 = (recall * precision)/ (recall + precision)
    return (recall,precision, acc,f1)

def remove_offset_value(traces):
    '''
    remove the offset value
    '''
    min_vals = np.min(traces, axis = 1) .reshape(-1,1)
    traces = traces - min_vals
    return traces

def plot_accuracy_pos_relative_amp(pred_labels, params):
    from scipy.interpolate import griddata
    
def create_grid(y_pred_forest, diffs, rel_amps,num_pos,num_rel_amp):
    
    max_diff = 150
    min_diff = 0
    
    max_relative_amp = 1
    min_relative_amp = 0
    
    my_grid = []
    counts = np.zeros(shape=(num_pos,num_rel_amp))
    for i in range(num_pos):
        my_grid.append([])
        for j in range(num_rel_amp):
            my_grid[i].append([])
    
    for i in range(len(y_pred_forest)):
        index1 = find_index(diffs[i], min_diff, max_diff, num_pos)
        index2 = find_index(rel_amps[i], min_relative_amp, max_relative_amp, num_rel_amp)
        my_grid[index1][index2].append(y_pred_forest[i])
    # calculate accuracies
    for i in range(num_pos):
        for j in range(num_rel_amp):
            if len(my_grid[i][j]) > 0:
                counts[i][j] = len(my_grid[i][j])
                my_grid[i][j] = sum(my_grid[i][j]) / len(my_grid[i][j])
            else:
                counts[i][j] = 0
                my_grid[i][j] = 0
    return my_grid, counts
        
def find_index(value,min_val, max_val, num_bins):
    if value >= max_val:
        return num_bins - 1
    if value <= min_val:
        return 0
    step = (max_val - min_val) / num_bins
    index = int((value - min_val) / step)
    if index >= num_bins:
        index = num_bins - 1
        
    return index

'''

    Train on synthetic data
    tune on real data (based on some ratio)
    Test on real data
    figure out hyperparams
    
'''
           
    
def main():
    import argparse
    
    # create parser for command-line arguments
    parser = argparse.ArgumentParser(description='Train classifiers using generated traces, tune and test using real data')
    parser.add_argument('in_file_train_X', metavar='in_file_train_X',
        type=str,
        help='Input file name for traces to use for training')
    parser.add_argument('in_file_train_Y', metavar='in_file_train_Y',
        type=str,
        help='Input file name  for labels of the traces')
    parser.add_argument('in_file_real_X', metavar='in_file_real_X',
        type=str,
        help='Input file name to locate the file for real traces (to use for test and validation)')
    parser.add_argument('in_file_real_Y', metavar='in_file_real_Y',
        type=str,
        help='Input file name to locate the file for labels of the real traces ')
    parser.add_argument('out_folder', metavar='out',
        type=str,
        help='Output folder for generated files.')
    
    parser.add_argument('-c', '--classifier', metavar='classifier', default=['RF'], nargs=1,
        type=str,
        help='Pick a classifier: RF, SVM, GBC')     
      
    parser.add_argument('-tune', '--tune', metavar='tune', default=[1], nargs=1,
        type=int,
        help='If 1, based on the split value, some portion of the real data will be used to tune the parameters \n \
        If 0, only one model will be trained based on the given parameters')  
     
    parser.add_argument('-tune_ratio', '--tune_ratio', metavar='tune_ratio', default=[0.2], nargs=1,
        type=float,
        help='Define what portion of the parameter space is searched')  

    # if not tuning, use these parameters
    parser.add_argument('-num_comp', '--num_comp', metavar='num_comp', default=[50], nargs=1,
        type=int,
        help='number of pca components')  
    parser.add_argument('-n_est', '--n_est', metavar='n_est', default=[100], nargs=1,
        type=int,
        help='number of estimators for random forest classifier')       
    parser.add_argument('-depth', '--depth', metavar='depth', default=[16], nargs=1,
        type=int,
        help='The maximum depth of the tree')       
    parser.add_argument('-min_split', '--min_split', metavar='min_split', default=[8], nargs=1,
        type=int,
        help='The minimum number of samples required to split an internal node')       
    parser.add_argument('-min_leaf', '--min_leaf', metavar='min_leaf', default=[8], nargs=1,
        type=int,
        help='The minimum number of samples required to be at a leaf node.')       
       
       
   
    parser.add_argument('-report', '--report', metavar='report', default=[1], nargs=1,
        type=int,
        help='Report the results')  

        
    parser.add_argument('--model_file', metavar='filename', default=['not_given'], nargs=1,
        type=str,
        help='Output file for the model (contains both classifier and pca). \n \
        If not given a name will be generated based on  the given params.')  
    

    # parse command-line arguments
    args = parser.parse_args()
    
    out_exists = os.path.exists(args.out_folder) 
    
    if out_exists:
        print("[WARNING]: given folder for output files already exists")
    else:
        print("[WARNING]: given folder for output files doesnt exist, it will be created!")
        path = args.out_folder
        try:
            os.mkdir(path)
        except OSError:
            print ("[ERROR] Creation of the directory %s failed" % path)
            sys.exit()
        else:
            print ("[INFO] Successfully created the directory %s " % path)
        
    # TODO: handle missing parameter here
    
    real_X = read_binary(args.in_file_real_X, -1, False)
    real_Y = read_labels(args.in_file_real_Y)
    
    train_X = read_binary(args.in_file_train_X,-1,False)
    train_Y = read_labels(args.in_file_train_Y)
    length = real_X.shape[1]
    
       # get rid of the offset 
    real_X = remove_offset_value(real_X)
    train_X = remove_offset_value(train_X)
    
    # normalize the traces
    real_X = preprocessing.normalize(real_X.reshape(-1,length))
    train_X = preprocessing.normalize(train_X.reshape(-1,length))
    
    # if tuning, num_com is the max. possible, 
    # if classifying, the given one
    if args.tune[0] == 1:
        pca = PCA(n_components=50,whiten=True)
    else:
        pca = PCA(n_components=args.num_comp[0],whiten=True)
    
    pca.fit(train_X) 
    real_X_tr = pca.transform(real_X)
    train_X_tr = pca.transform(train_X)

    
    if args.tune[0] == 1:
        all_results = tune_random_forest(train_X_tr, train_Y,real_X_tr, real_Y, tune_ratio=args.tune_ratio[0])
        out_filename = args.out_folder + '/tune_{}_results.csv'.format(args.classifier[0])    
        np.savetxt(out_filename, all_results, header='num_comp,n_est,depth,leaf,split,recall,precision,acc,f1',delimiter=',')
        
    else:
        model = random_forest(train_X_tr, train_Y, args.n_est[0],args.depth[0],args.min_leaf[0],args.min_split[0])
        y_pred = model.predict(real_X_tr)
        cm = metrics.confusion_matrix(real_Y, y_pred)
        (recall,precision, acc,f1) = get_metrics(cm)   
        print('recall:{0:.3f}, precision:{1:.3f}, accuracy:{2:.3f}, f1:{3:.3f}'.format(recall,precision, acc,f1))
        cm_out_file = '{}/confusion_matrix.pdf'.format(args.out_folder)
        plot_confusion_matrix(cm, ['Single Pulse', 'Double Pulse'],
                          normalize=False,
                          title='Confusion matrix, recall:{0:.3f}, precision:{1:.3f}, accuracy:{2:.3f}, f1:{3:.3f}'.format(recall,precision, acc,f1),
                          out_file=cm_out_file)
        # Output a pickle file for the model
        if args.model_file[0] == 'not_given':
            model_name = "{}/model_num_comp_{}_n_est_{}_depth_{}_min_leaf_{}_min_split_{}.pkl".format(args.out_folder,args.num_comp[0],args.n_est[0],args.depth[0],args.min_leaf[0],args.min_split[0])
        else:
            model_name = args.model_file[0]
        fill_model = [pca,model]
        joblib.dump(fill_model, model_name) 
        
                    

if __name__ == "__main__":
    main() 