#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Aug 17 14:20:33 2019

@author: cagri


From the parameters file, get rid of the unnecessary parameters and split the binary data
tp single and double pulse files to use in the other steps in the pipeline. 
"""

from helper import read_fit_params,classify_with_chisquare,read_binary,write_binary,write_labels
import numpy as np
import os.path
import os
import sys

def process_data(traces, params):
    # use the fitting results to classify the data
    labels = classify_with_chisquare(params)
    single_traces =[]
    single_params = []
    double_traces = []
    double_params = []
    #  
    for i in range(len(labels)):
        label = labels[i]
        trace = traces[i]
        param = params[i]
        if label == 1:
            double_traces.append(trace)
            # pos1,a1, k1, k2,pos2, a2,k3,k4, o
            double_params.append(param[11:])
        else:
            #pos1,a1, k1, k2, o
            single_traces.append(trace)
            single_params.append(param[3:8])
            
    single_traces = np.array(single_traces)
    double_traces = np.array(double_traces)
    single_params = np.array(single_params)
    double_params = np.array(double_params)
    
    return single_traces,single_params,double_traces,double_params,labels
        
    

def main():
    import argparse
    
    # create parser for command-line arguments
    parser = argparse.ArgumentParser(description='Classify the data using fitting results')
    parser.add_argument('in_fit_file', metavar='in_fit_file',
        type=str,
        help='File name for the fitting results')
    parser.add_argument('in_bin_file', metavar='in_bin_file',
        type=str,
        help='File name for the binary trace file used to produce fitting results') 
    parser.add_argument('out_folder', metavar='out_folder',
        type=str,
        help='Output folder for generated files.') 
        
    args = parser.parse_args()
    
    out_exists = os.path.exists(args.out_folder) 
    
    if out_exists:
        print("[WARNING]: given folder for output files already exists")
    else:
        print("[WARNING]: given folder for output files doesnt exist, it will be created!")
        path = args.out_folder
        try:
            os.mkdir(path)
        except OSError:
            print ("[ERROR] Creation of the directory %s failed" % path)
            sys.exit()
        else:
            print ("Successfully created the directory %s " % path)
        
    params = read_fit_params(args.in_fit_file)  
    traces = read_binary(args.in_bin_file, -1, False)
    print('[INFO] Input files are read!')
    single_traces,single_params,double_traces,double_params,labels = process_data(traces, params)
    # write params to file (after removing unnec)
    np.savetxt('{}/real_single_params.txt'.format(args.out_folder), single_params)
    np.savetxt('{}/real_double_params.txt'.format(args.out_folder), double_params)
    print('[INFO] Parameter files are created: \n \
          parameter file for single pulses contains: pos1, a1, k1, k2, o \n \
          parameter file for double pulses contains: pos1, a1, k1, k2, pos2, a2, k3, k4, o')
    count_d = min(len(double_traces),len(single_traces))
    mixed_data = np.concatenate((double_traces[:count_d], single_traces[:count_d]), axis=0)
    mixed_data_labels = [1] * count_d
    mixed_data_labels.extend([0] * count_d)

    write_binary(single_traces, '{}/real_single_traces.bin'.format(args.out_folder))
    write_binary(double_traces, '{}/real_double_traces.bin'.format(args.out_folder))
    print('[INFO] Trace files are created in binary format')
    write_binary(mixed_data, '{}/real_mixed_data.bin'.format(args.out_folder))
    write_labels(mixed_data_labels,'{}/real_mixed_data_labels.txt'.format(args.out_folder))
    print('[INFO] Mixed data files are created to use for testing and tuning of the classifiers \n \
          It contains all the double pulses from the given binary file and that many single pulses')
   
    
    
if __name__ == "__main__":
    main() 
    
    
    
    
    