from pulse_maker import * 
import numpy as np
import sys
from helper import *
import os.path
import os
from data_generator import generate_traces_from_params




def generate_params_from_histograms(hist_p1,hist_a1,hist_k1,hist_k2,hist_p2,hist_a2,hist_k3,hist_k4,hist_o,count):
    '''
    Generate parameters from given histogram data
    
    '''
    import scipy.stats
    
    # create the distributions from the histograms   
    dist_p1 = scipy.stats.rv_histogram(hist_p1)
    dist_a1 = scipy.stats.rv_histogram(hist_a1)
    dist_k1 = scipy.stats.rv_histogram(hist_k1)
    dist_k2 = scipy.stats.rv_histogram(hist_k2)
    dist_p2 = scipy.stats.rv_histogram(hist_p2)
    dist_a2 = scipy.stats.rv_histogram(hist_a2)
    dist_k3 = scipy.stats.rv_histogram(hist_k3)
    dist_k4 = scipy.stats.rv_histogram(hist_k4)
    dist_o = scipy.stats.rv_histogram(hist_o)
    dist_list = [dist_p1,dist_a1,dist_k1,dist_k2,dist_p2,dist_a2,dist_k3,dist_k4,dist_o]
    
    params = np.ndarray((count, 9)) # num_params = 9
    # generate parameters
    for i in range(len(dist_list)):
        vals = dist_list[i].rvs(size=count)
        params[:,i] = vals
        
    return params
       


def read_param_file(filename):
    '''
    Read value-probability pairs from the given file
    
    output format: tuple(np.array of probabilities, np.array of values)
    '''
    fptr = open(filename, 'r')
    # read the file
    lines = fptr.readlines()
    lines[0] = lines[0].replace(" ", "")
    lines[1] = lines[1].replace(" ", "")
    values = lines[1].strip().split(',')
    probs = lines[0].strip().split(',')
    #conversion
    probs = [float(prob) for prob in probs]
    values = [float(val) for val in values]
    if len(probs) + 1 != len(values):
        sys.exit('[ERROR] length of the probabilities list and values list dont match \n \
                 length of the probabilities list should be n and \n \
                 length of the values list should be n + 1')        
    
    res_tuple = (np.array(probs), np.array(values))
    
    return res_tuple
    

def main():
    import argparse
    # create parser for command-line arguments
    parser = argparse.ArgumentParser(description='Generate synthetic data to a double pulse classifier')
    parser.add_argument('position1', metavar='position1',
        type=str,
        help='Input file for position-1 parameter') 
    parser.add_argument('amplitude1', metavar='amplitude1',
        type=str,
        help='Input file for amplitude-1 parameter') 
    parser.add_argument('steepness1', metavar='steepness1',
        type=str,
        help='Input file for steepness-1 parameter') 
    parser.add_argument('decay1', metavar='decay1',
        type=str,
        help='Input file for decay-1 parameter') 
    parser.add_argument('position2', metavar='position2',
        type=str,
        help='Input file for position-2 parameter') 
    parser.add_argument('amplitude2', metavar='amplitude2',
        type=str,
        help='Input file for amplitude-2 parameter') 
    parser.add_argument('steepness2', metavar='steepness2',
        type=str,
        help='Input file for steepness-2 parameter')
    parser.add_argument('decay2', metavar='decay2',
        type=str,
        help='Input file for decay-2 parameter')
    parser.add_argument('offset', metavar='offset',
        type=str,
        help='Input file for offset parameter')
    parser.add_argument('out_file', metavar='out_file',
        type=str,
        help='Output file for generated pulses.') 
    
    parser.add_argument('-l', '--length', metavar='length', default=[250], nargs=1,
        type=int,
        help='Length of the traces to produce (Default: 250)') 
    
    parser.add_argument('-c', '--count', metavar='count', default=[50000], nargs=1,
        type=int,
        help='Number of double pulses to generate (Default: 50000)')
    
    parser.add_argument('-noise', '--noise', metavar='noise', default=[0], nargs=1,
        type=int,
        help='Whether to add noise to produced signals \n \
        (std and mean values should be provided to add noise) (Default: 0)')
    parser.add_argument('-mean', '--mean', metavar='mean', default=[0], nargs=1,
        type=float,
        help='Mean value for the Gaussian Distribution used to produce noise (Default: 0)')
    parser.add_argument('-std', '--std', metavar='std', default=[3.38], nargs=1,
        type=float,
        help='Standard deviation for the Gaussian Distribution used to produce noise (Default: 3.38)')      
    parser.add_argument('-w_p', '--write_params', metavar='write_params', default=[1], nargs=1,
        type=int,
        help='To write generated parameters to files as well as generated signals (Default: 250)')
    # parse command-line arguments
    args = parser.parse_args()    
    
    hist_p1 = read_param_file(args.position1)
    hist_a1 = read_param_file(args.amplitude1)
    hist_k1 = read_param_file(args.steepness1)
    hist_k2 = read_param_file(args.decay1)
    hist_p2 = read_param_file(args.position2)
    hist_a2 = read_param_file(args.amplitude2)
    hist_k3 = read_param_file(args.steepness2)
    hist_k4 = read_param_file(args.decay2)
    hist_o = read_param_file(args.offset)
    
    params = generate_params_from_histograms(hist_p1,hist_a1,hist_k1,hist_k2,hist_p2,hist_a2,hist_k3,hist_k4,hist_o,args.count[0])
    double_traces = generate_traces_from_params(params, False, args.length[0], False)
    if args.noise[0] == 1: 
        std = args.std[0]
        mean = args.mean[0]
        for i in range(len(double_traces)):
            double_traces[i] = add_noise(double_traces[i], mean=mean, std=std)

    write_binary(double_traces, args.out_file)
    
    if args.write_params[0] == 1:
        np.savetxt('{}.params'.format(args.out_file), params)            
 
    
if __name__ == "__main__":
    main()        
            